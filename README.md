# fei
## Datensorglosigkeitsmessgerät
Die Erweiterung klassifiziert, wie oft Kontakt zu seitenfremden Servern aufgebaut werden soll. Diese Zählung wird nach Ähnlichkeit gewichtet, so dass eine Anfrage von example.com aus an cdn-example.com nicht so stark ins Gewicht fällt wie z.B. eine Anfrage an google.com. Der so gewichtete Score liefert einen ersten Anhaltspukt dafür, wie sorglos die Seite mit Deinen Daten umgeht.

Browser wie Firefox haben eingebaute Trackerblocker und es gibt natürlich auch Erweiterungen, die solche Anfragen blockieren. "fei" zählt alle *versuchten* Serveranfragen, bevor die Blocker und Erweiterungen über deren Ausführung entscheiden. Wir denken, dass dies das Sorglosigkeitsmaß des Seitenbetreibers besser wiedergibt. Allerdings sind solche
Tracker oft kaskadierend: Javascript vom ersten Server lädt Code nach, der Anfragen an andere Server generiert... Wenn Du also solche Blocker oder Erweiterungen aktiviert hast, wird der Score einer Webseite meist deutlich kleiner sein als ohne deren Aktivierung, jedoch meist nicht 0, sogar, wenn in Wirklichkeit alle Drittanfragen geblockt werden.

Dieser Score kann während des Besuchs einer Seite anwachsen, auch wenn man gerade nicht aktiv auf der Seite ist. Das sind Anfragen, die dem Tracker anzeigen sollen, dass der Nutzer die Seite immer noch geöffnet hat und schicken so jedes Mal neue persönliche Information an den Tracker ("der is fei immer noch net fertig"). "fei" zählt diese Datenpakete bewusst mit.

Der Score wird pro Domainname ohne TLD erhoben. Dadurch werden Anfragen z.B. von example.com an example.net oder auch an cdn.example.com als intern, also mit Gewicht 0, gewertet.

Für eine genauere Analyse und Steuerung des eigenen Netzwerkverkehrs gibt es bereits sehr gute andere Erweiterungen. "fei" dient dazu, auf das Problem aufmerksam zu machen.

## Wir erheben keine Daten und geben keine Daten an Dritte weiter
Die Erweiterung erzeugt keinen zusätzlichen Netzwerkverkehr, weder zu unseren noch anderen Servern. Insbesondere werden die besuchten Seiten nicht mit Trackerlisten o.ä. abgeglichen. Der Score jeder Seite wird alleine auf Deinem Gerät errechnet und
gespeichert. Mit der Löschung Deines Webspeichers werden auch diese Daten gelöscht.

## Was wird mir angezeigt?
In der Adressezeile erscheint ein Tiersymbol (s. Konfiguration), das den momentanen Stand des Score klassifiziert. Durch Klicken auf das Symbol erhältst Du den genauen Score und eine kurze Erläuterung dazu.

Folgendes kann zusätzlich eingestellt werden: Sind auf der Seite Links zu Drittseiten, die Du schon besucht hast (und den Cache seitdem nicht geleert), siehst Du am linken Rand des Links das entsprechende Symbol, das dem durchschnittichen Score der (bis zu 5 letzten) Messungen auf der Seite des Links entspricht. Standardmäßig ist diese Funktion nicht aktiv, weil sie das Layout einer Seite manchmal zu stark verändern kann.

## Konfiguration
Die Klassengrenzen können in den Einstellungen der Erweiterung geändert werden. Die Skala ist logarithmisch, weil die Anzahl von Anfragen typischerweise exponentiell mit der Sorglosigkeit der Seitenbetreiber wächst. So sind die Defaultgrenzen
* Fisch: 0
* Vogel: bis 5
* Katze: bis 50
* Zikade: ab 50

Das Markieren bereits klassifizierter Seiten an entsprechenden Links kann aktiviert werden. Standardmäßig ist dies seit Version 1.0.6 deaktiviert, weil der Einfluss auf das Seitenlayout nicht vollständig absehbar ist.

## "fei"
*fei* ist fränkisch und eigentlich unübersetzbar, aber ich versuch's mal: es steht für eine adverbiale Betonung, oft mit dem Unterton des Erstaunens oder Mitgefühls, z.B. *fei net*: "Nein, auch, wenn Du das anders erwartet hast" oder "Es schmerzt mich, das zu sagen, aber: nein". Auf HTML heißt *fei* in etwa <code>&lt;em&gt;</code>. Die Erweiterung heißt "fei", weil die Information, welche Seiten viel und welche wenig Daten weitergeben (wollen), recht überraschend sein kann und die Erweiterung nichts anderes tut als darauf aufmerksam zu machen.

## Quellen
Die Grafiken für die einzelnen Klassen sind Derivate freier Cliparts aus [https://openclipart.org](https://openclipart.org), genauer:
* Fisch: von chikiyo, [https://openclipart.org/detail/134725/fish](https://openclipart.org/detail/134725/fish)
* Vogel: von meneya, [https://openclipart.org/detail/281500/sparrow](https://openclipart.org/detail/281500/sparrow)
* Katze: von chikiyo, [https://openclipart.org/detail/133789/black-cat](https://openclipart.org/detail/133789/black-cat)
* Zikade: von j4p4n, [https://openclipart.org/detail/316983/simple-cicada](https://openclipart.org/detail/316983/simple-cicada) 
