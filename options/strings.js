const _available_lang = ["de","en"];

const _strings = { 
	"de": {
		"comments": "Ziehen, um die Klassenbereiche zu ändern.",
		"evallinks": "Frühere Klassifizierung von Links auf den Seiten anzeigen"
	},
	"en": {
		"comments": "Drag to change the class ranges.",
		"evallinks": "Show former classification of links on web pages"
	}
}

//determine language
//default to english if language is not available
var _lang = navigator.language;
if ( ! _available_lang.includes(_lang) ) { _lang = "en"; }
