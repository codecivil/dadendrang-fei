const default_limit = { "_limit": { "bird": 5, "cat": 10, "cicada": 10 } };
const default_evallinks = { "_evallinks": false };

var _limit = new Object();

function updateLimit(el) {
	el.parentElement.querySelectorAll('.area').forEach(_area => {
		switch(_area.id) {
			case "bird": 
				_area.textContent = Math.floor(_limit["bird"]+0.5);
				break;
			case "cat": 
				_area.textContent = Math.floor(_limit["bird"]*_limit["cat"]+0.5);
				break;
//			case "cicada": 
//				_area.textContent = Math.floor(_limit["bird"]*_limit["cat"]*_limit["cicada"]+0.5);
//				break;
		}
	});
	browser.storage.local.set({"_limit": _limit});
}

function updateEvalLinks(evt) {
	browser.storage.local.set({"_evallinks": document.querySelector('#evalLinks').checked });
}

function showLimits() {
	browser.storage.local.get(default_limit).then(_lim => {
		document.querySelectorAll('.area').forEach(_area => {
			_area.style.width = 100*Math.log(_lim._limit[_area.id])/Math.log(500)+"%";
		});
	});
}

function showEvalLinks() {
	browser.storage.local.get(default_evallinks).then(_evl => {
		document.querySelector('#evalLinks').checked = _evl._evallinks;
	});
}

const resizeObeserver = new ResizeObserver((entries) => {
	for (const entry of entries) {
		_limit[entry.target.id] = Math.exp(Math.log(500)*(entry.borderBoxSize[0].inlineSize/parseFloat(getComputedStyle(entry.target.parentElement).width.replace('px',''))-1))*500;
		updateLimit(entry.target);
	}
});

//translate sections
document.querySelectorAll('.translate').forEach(_translate => {
	_translate.textContent = _strings[_lang][_translate.dataset.abstract];
});

//show settings
showLimits();
showEvalLinks();

//set listeners
document.querySelectorAll('.area').forEach(_area => resizeObeserver.observe(_area));
document.querySelector('#evalLinks').addEventListener("change",updateEvalLinks);
