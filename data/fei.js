function evalLinks() {
	browser.storage.local.get({enabled: true}).then(_enabled => {
		if ( ! _enabled.enabled ) { return false; }
		let _here = getDomain(window.location.href);
		document.querySelectorAll('a').forEach(a => {
			let _domain = getDomain(a.href);
			if ( _domain == _here ) { return false; }
			browser.storage.local.get(_domain).then(_dlog => {
				if ( _dlog[_domain] == undefined ) { return false; }
				let _mu = 0; let _count = 0;
				Object.keys(_dlog[_domain]).forEach(_logkey => { _count += 1; _mu += ((_count -1)*_mu+_dlog[_domain][_logkey])/_count; });
				//get correct icon and add it to link
				classifyScore(_mu).then(_thisicon => {
					let _icon = document.createElement('span');
					_icon.style.display = "inline-block";
					_icon.style.width = "1rem";
					_icon.style.height = "1rem";
					_icon.style.position = "relative";
					_icon.style.left = "1rem";
					_icon.style.zIndex = -1;
					_icon.style.background = "linear-gradient(#ffffff40,#ffffff40), center right / contain url("+browser.runtime.getURL('icons/'+_thisicon.iconname+'.svg')+") no-repeat";
					_icon.title = _thisicon.title;
					a.prepend(_icon);
				});					
			}, _dlog => { return false; });
		});
	});
}
var _urlobj = new Object();

//evaluate Links if user chose so
browser.storage.local.get({_evallinks: false}).then(evl => {
	if ( evl._evallinks ) {
		setTimeout(evalLinks,700);
	}
});
//_enable();
