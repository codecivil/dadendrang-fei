browser.windows.getCurrent({populate: true}).then( _window => {
	_window.tabs.forEach(tab => {
		if ( ! tab.active ) { return; }
		browser.storage.local.get('enabled').then(_enabled => {
			if ( ! _enabled.enabled ) { return false; }
			browser.storage.local.get('current').then(_current => {
				_score =  _current.current[tab.id];
				document.querySelector('div').setAttribute('lang',navigator.language);
				document.querySelector('.score').textContent = Math.floor(_score+0.5);
				document.querySelector('.score').style.color = 'hsl('+(90-_score/(_score+50)*150)+'deg 100% 50%)';

				document.querySelector('div').classList = '';
				const default_limit = { "_limit": { "bird": 5, "cat": 10, "cicada": 10 } };
				let _limit = new Object();
				browser.storage.local.get(default_limit).then( _lim => {
					_limit.bird = _lim._limit.bird;
					_limit.cat = _lim._limit.bird*_lim._limit.cat;
					if ( _score == 0 ) { document.querySelector('div').classList.add('fish'); }
					if ( _score > 0 && _score < _limit.bird ) { document.querySelector('div').classList.add('bird'); }
					if ( _score >= _limit.bird && _score < _limit.cat ) { document.querySelector('div').classList.add('cat'); }
					if ( _score >= _limit.cat ) { document.querySelector('div').classList.add('cicada'); }
				});
			});
			document.body.hidden = false;
		});
	});
});
