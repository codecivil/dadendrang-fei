//const _regex = /http[s]?:\/[\/]?[^\/]*[\/\.]([^\.\/\?]+\.[^\.\/\?]+)\/.*$/; //this is the domain
const _regex = /http[s]?:\/[\/]?[^\/\.]*[\/\.]([^\.\/\?]+)\.[^\.\/\?]+\/.*$/; //this is the domain without tld
function getDomain(_str) {
	let _return;
	_return = (_str+'/').match(_regex);
	if ( _return && _return[1] ) { return _return[1]; } else { return ''; }
}

//keep only last _length number of keys
function noMoreThan(_length,_obj) {
	let _keys = Object.keys(_obj).reverse().slice(0,_length);
	let _newobj = new Object();
	_keys.forEach(_key => { _newobj[_key] = _obj[_key]; });
	return _newobj;
}

function classifyScore(_score) {
	const default_limit = { "_limit": { "bird": 5, "cat": 10, "cicada": 10 } };
	let _limit = new Object();
	return browser.storage.local.get(default_limit).then( _lim => {
		_limit.bird = _lim._limit.bird;
		_limit.cat = _lim._limit.bird*_lim._limit.cat;
		if ( _score == 0 ) { _pageActionIconName = "dadendrang_no"; _pageActionTitle = _strings[_lang].icon_fish; }
		if ( _score > 0 && _score < _limit.bird ) { _pageActionIconName = "dadendrang_some"; _pageActionTitle = _strings[_lang].icon_bird; }
		if ( _score >= _limit.bird && _score < _limit.cat ) { _pageActionIconName = "dadendrang_nosy"; _pageActionTitle = _strings[_lang].icon_cat; }
		if ( _score >= _limit.cat ) { _pageActionIconName = "dadendrang_olala"; _pageActionTitle = _strings[_lang].icon_cicada; }
		return { "iconname": _pageActionIconName, "title": _pageActionTitle }
	});
}
