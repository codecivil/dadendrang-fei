var _urlobj = new Object();
var _tabobj = new Object();

//const _regex = /http[s]?:\/[\/]?[^\/]*[\/\.]([^\.\/\?]+\.[^\.\/\?]+)\/.*$/; //this is the domain

//ratio of longest substring of string1 contained in string2
function containsMax(string1,string2) {
	string1 = string1.substr(0,1)+string1.substr(1).toLowerCase();
	string2 = string2.substr(0,1)+string2.substr(1).toLowerCase();
	var sim = 0;
	var _index = 0;
	while ( _index < string1.length ) {
		var contained = true;
		var _length = 0;
		var _index2new = 0;
		while ( contained ) {
			_index2 = _index2new;
			_length += 1;
			_index2new = string2.indexOf( string1.substr(_index,_length) );
			if ( _index2new == -1 || _length + _index > string1.length ) { contained = false; }		
		}
		sim = Math.max(sim,_length-1);
		_index += 1;
	}
	sim = sim/string1.length;
	return sim;
}

function _enable() {
	browser.storage.local.get({enabled: true}).then(_enabled => {
		let _newtruth = ! _enabled.enabled;
		browser.browserAction.setIcon({
			path: {
				16: "../icons/fei_logo_"+_newtruth+".svg",
				32: "../icons/fei_logo_"+_newtruth+'.svg'
			}
		});
		browser.storage.local.set({enabled: _newtruth});
		if ( _newtruth ) {
			browser.webRequest.onBeforeRequest.addListener(logRequest, {
			  urls: ["<all_urls>"],
			});
			browser.tabs.query({}).then(tabs => tabs.forEach(tab => {
				browser.pageAction.show(tab.id);
			}));
			//reset "current"
			browser.storage.local.set({'current': {}});
		} else {
			browser.webRequest.onBeforeRequest.removeListener(logRequest, {
			  urls: ["<all_urls>"],
			});
			browser.tabs.query({}).then(tabs => tabs.forEach(tab => {
				browser.pageAction.hide(tab.id);
			}));
		}
	});
}

browser.browserAction.onClicked.addListener(_enable);
_enable(); _enable(); //twice to reestablish user's setting
let _key;

async function logRequest(requestDetails) {
	let _date = Math.floor(Date.now()/86400000)*86400000; 
	//assign requests to hostname
	let _target;
	//oiriginal call of site has undefined documentUrl
	if ( requestDetails.documentUrl == undefined ) {
		_key = getDomain(requestDetails.url);
		_target = _key;
		_urlobj[_key] = new Object();
		_urlobj[_key][_date] = 0;
		//log tab in order to track later requests to their origin
		_tabobj[requestDetails.tabId] = _key;
	} else {
		if ( ! (requestDetails.documentUrl+'/').match(_regex) ) { console.log('no match:',requestDetails.documentUrl); }
		//_key = (requestDetails.documentUrl+'/').match(_regex)[1];
		_target = getDomain(requestDetails.url);
		_key = _tabobj[requestDetails.tabId];
	}
	if ( _urlobj[_key] == undefined ) { _urlobj[_key] = new Object(); _urlobj[_key][_date] = 0; }
	//count request url according to similarity of documentUrl; so 0 if it is the same domain, 1 if it is totally different
	let _weight = 1 - containsMax(_key,_target);
	//reduce weight to 0 if it is not a thord party request
	if ( ! requestDetails.thirdParty ) { _weight = 0; }
	//sum up weights
	_urlobj[_key][_date] += _weight;
	//update pageAction icon
	classifyScore(_urlobj[_key][_date]).then( _pageAction => {
		browser.pageAction.setIcon({ path: "icons/"+_pageAction.iconname+'.svg', tabId: requestDetails.tabId });
		browser.pageAction.setTitle({ title: _pageAction.title, tabId: requestDetails.tabId });
	});
	//store current and new value
	browser.storage.local.get({'current': {}}).then(_current => {
		_current.current[requestDetails.tabId] = _urlobj[_key][_date];
		browser.storage.local.set(_current);
	});
	let _keyobj = new Object();
	_keyobj[_key] = new Object();
	_keyobj[_key][_date] = 0;
	browser.storage.local.get(_keyobj).then(item => {
		//on success
		//throw old entries away and keep only 5
		item = noMoreThan(5,item);
		//
		if ( item[_key] != undefined && item[_key][_date] != undefined ) {
			item[_key][_date] = Math.max(item[_key][_date],_urlobj[_key][_date]);
		} else {
			item[_key] = new Object();
			item[_key][_date] = 0;
		}
		browser.storage.local.set(item);
	},item => {	
		//on error
	});
}

