const _available_lang = ["de","en"];

const _strings = { 
	"de": {
		"icon_fish": "Stumm wie ein Fisch",
		"icon_bird": "Leichtes Gezwitscher",
		"icon_cat": "Neugierig wie eine Katze",
		"icon_cicada": "Laut wie eine Zikade"
	},
	"en": {
		"icon_fish": "Mute like a fish",
		"icon_bird": "Chirpy like a bird",
		"icon_cat": "Nosy like a cat",
		"icon_cicada": "Noisy like a cicada"
	}
}

//determine language
//default to english if language is not available
var _lang = navigator.language;
if ( ! _available_lang.includes(_lang) ) { _lang = "en"; }
